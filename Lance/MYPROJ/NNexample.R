library(stats)

iris = read.csv('../DATA/iris.csv', header=F)

sig <- function(l) {
    out = 1:length(l)
    for (i in 1:length(l)) {
	out[i] <- 1.0/(1 + exp(-l[i]))
    }
    out
}

sigDiv <- function(x) {
       sig(x)/(1 - sig(x))
}

NN1_sig <- function(till, X, Y) {
	l0 = X
	w = stats::runif(4, 0, 1)
	while (till != 0) {
	      l1 = sig(w %*% t(l0))             #1x4 * 4x100
	      l1_error = Y - l1                 #1x100
 	      temp = l1_error * sigDiv(l1)      #1x100
	      if (till %% 10 == 0) {
	      	 message(mean(l1_error))
	      }
	      l1_delta = t(l0) %*% temp         #4x100 * 100x1
	      message()
	      message("l1_delta")
	      message(l1_delta)
	      message(w)
	      message(w + l1_delta)
	      message()
	      w = t(w + l1_delta)               #1x100 + 1x100
	      message(w)
	      till = till -1
	}
	message(l1)
	w
}

mapbin <- function(l) {
    out = 1:length(l)
    for (i in 1:length(l)) {
    	if (l[i] == 'Iris-setosa') {
	  out[i] <- 0
	} else {
	  out[i] <- 1
	}
    }
    out
}

X = iris[,0:4]
Y = mapbin(iris[,5])
NN1_sig(100, X, Y)