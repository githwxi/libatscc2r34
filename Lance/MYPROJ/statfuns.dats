(* ****** ****** *)
//
// LG-2017-10:
// A running example
// from ATS2 to R(stat)
//
(* ****** ****** *)
//
#define
ATS_MAINATSFLAG 1
#define
ATS_DYNLOADNAME "my_dynload"
//
(* ****** ****** *)
//
#define
LIBATSCC2R34_targetloc
"$PATSHOME/contrib/libatscc2r34"
//
(* ****** ****** *)
//
#include "{$LIBATSCC2R34}/mylibies.hats"
//
(* ****** ****** *)
//
staload "./../MYLIB/mylib.sats"
//
(* ****** ****** *)
//
//  Testing dotprod and crossprod
//
val xs =
R34vector_tabulate_fun{int}(10, lam(i) => i)
val ys = 
R34vector_tabulate_fun{int}(10, lam(i) => (i+1)*(i+1))
//
val xss = 
R34matrix_tabulate_fun{int}(10, 10, lam(i, j) => i*j)
val yss = 
R34matrix_tabulate_fun{int}(10, 10, lam(i, j) => 2*i*j)
//
val () = $extfcall(void, "message", "xs = ", xs)
val () = $extfcall(void, "message", "ys = ", ys)
//
val xsxs = dotprod_R34vector_R34vector(xs, xs)
val ysxs = dotprod_R34vector_R34vector(ys, xs)
val xsys = dotprod_R34vector_R34vector(xs, ys)
//
val () = $extfcall(void, "message", "dot(xs, xs) = ", xsxs)
val () = $extfcall(void, "message", "dot(ys, xs) = ", ysxs)
val () = $extfcall(void, "message", "dot(xs, ys) = ", xsys)
//
val xsxs = crossprod_R34vector_R34vector(xs, xs)
val ysxs = crossprod_R34vector_R34vector(ys, xs)
val xsys = crossprod_R34vector_R34vector(xs, ys)
//
val () = $extfcall(void, "message", "cross(xs, xs) = ", xsxs)
val () = $extfcall(void, "message", "cross(ys, xs) = ", ysxs)
val () = $extfcall(void, "message", "cross(xs, ys) = ", xsys)
//
(* ****** ****** *)
//
//  Testing omit and sample
//

val () = assertloc(length(xs) >1)
val nxs = omit_nan_R34vector(xs)
val () = $extfcall(void, "message", "nanOmit(xs) = ", nxs)
//
// val sxs = sample_without_rep_R34vector(xs, 15)
val sxs = sample_without_rep_R34vector(xs, 10)
val sys = sample_with_rep_R34vector(ys, 15)
val () = $extfcall(void, "message", "sample_without_rep(xs) = ", sxs)
val () = $extfcall(void, "message", "sample_with_rep(ys) = ", sys)
//

val df = $extfcall(R34dframe(double), "read.table", "../DATA/oldfaith.txt")
val () = assertloc(nrow(df) >10)
val () = assertloc(ncol(df) >0)
val () = $extfcall(void, "message", "df = ", df)
//
val xs = omit_nan_R34dframe(df)
val () = $extfcall(void, "message", "nanOmit(df) = ", df)
//
val sdf = sample_without_rep_R34dframe(df, 10)
val swdf = sample_with_rep_R34dframe(df, 10)
val () = $extfcall(void, "message", "sample_without_rep(df) = ", sdf)
val () = $extfcall(void, "message", "sample_with_rep(df) = ", swdf)
//

%{^
######
if
(!
(exists("libatscc2r34.is.loaded"))
)
{
  assign("libatscc2r34.is.loaded", FALSE)
}
######
if
(
!(libatscc2r34.is.loaded)
)
{
  sys.source("./../MYLIB/libatscc2r34_all.R"); sys.source("./../MYLIB/mylib_cats.R")
}
######
%} // end of [%{^]

(* ****** ****** *)

%{$
my_dynload()
%} // end of [%{$]

(* ****** ****** *)

(* end of [statfuns.dats] *)
