//
#define
LIBATSCC2R34_targetloc
"$PATSHOME/contrib/libatscc2r34"
//
(* ****** ****** *)
//
#include "{$LIBATSCC2R34}/mylibies.hats"
//
(* ****** ****** *)

(*
fun
{a:t@ype}
R34vector_median
{n:pos}(R34vector(a, n)): double
//
overload median with R34vector_median
//
*)

(* ****** ****** *)

(*
fun
cbind_R34vector_R34vector
{a:t@ype}{n:pos}
( xs: R34vector(a, n)
, ys: R34vector(a, n)): R34matrix(a, n, 2) = "mac#cbind_R34vector_R34vector"
fun
rbind_R34vector_R34vector
{a:t@ype}{n:pos}
( xs: R34vector(a, n)
, ys: R34vector(a, n)): R34matrix(a, 2, n) = "mac#rbind_R34vector_R34vector"
//
overload cbind with cbind_R34vector_R34vector
overload rbind with rbind_R34vector_R34vector
//
*)
 
(* ****** ****** *)

fun
dotprod_R34vector_R34vector
{a:t@ype}{n:pos}
( xs: R34vector(a, n)
, ys: R34vector(a, n)): double = "mac#dotprod_R34vector_R34vector"

fun
crossprod_R34vector_R34vector
{a:t@ype}{n:pos}
( xs: R34vector(a, n)
, ys: R34vector(a, n)): R34matrix(double, n, n) = "mac#crossprod_R34vector_R34vector"


fun
omit_nan_R34vector
{a:t@ype}{n:pos}
( xs: R34vector(a, n) ): [l:nat | l <=n] R34vector(a, l)= "mac#omit_nan_R34vector"
// need to check that there are still some values left after omitting NaNs

fun
sample_without_rep_R34vector
{a:t@ype}{n:pos}{m:pos|m <=n}
(xs : R34vector(a, n), m: int(m)): R34vector(a, m) = "mac#sample_without_rep_R34vector"
// cannot sample more than n

fun
sample_with_rep_R34vector
{a:t@ype}{n:pos}{m:pos}
(xs : R34vector(a, n), m:int(m)): R34vector(a, m) = "mac#sample_with_rep_R34vector"
//

fun
omit_nan_R34dframe
{a:t@ype}{n:pos}{m:pos}
( xs: R34dframe(a, m, n) ): [m1:nat| m1 <= m] R34dframe(a, m1, n) = "mac#omit_nan_R34dframe"

fun
sample_without_rep_R34dframe
{a:t@ype}{n:pos}{m:pos}{s:pos|s <=m}
(xs : R34dframe(a, m, n), s: int(s)): R34dframe(a, s, n) = "mac#sample_without_rep_R34dframe"
// cannot sample more than n

fun
sample_with_rep_R34dframe
{a:t@ype}{n:pos}{m:pos}{s:pos}
(xs : R34dframe(a, m, n), s:int(s)): R34dframe(a, s, n) = "mac#sample_with_rep_R34dframe"