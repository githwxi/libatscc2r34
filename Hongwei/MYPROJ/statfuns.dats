(* ****** ****** *)
//
// HX-2017-10:
// A running example
// from ATS2 to R(stat)
//
(* ****** ****** *)
//
#define
ATS_MAINATSFLAG 1
#define
ATS_DYNLOADNAME "my_dynload"
//
(* ****** ****** *)
//
#define
LIBATSCC2R34_targetloc
"$PATSHOME/contrib/libatscc2r34"
//
(* ****** ****** *)
//
#include "{$LIBATSCC2R34}/mylibies.hats"
//
(* ****** ****** *)

staload "./../MYLIB/mylib.sats"

(* ****** ****** *)
//
val xs =
R34vector_tabulate_fun
  {int}(10, lam(i) => (i+1)*(i+1))
val xss =
R34matrix_tabulate_fun
  {int}(10, 10, lam(i,j) => (i+1)*(j+1))
//
(* ****** ****** *)
//
val () = $extfcall(void, "message", "mean = ", mean(xs))
val () = $extfcall(void, "message", "median = ", median(xs))
val () = $extfcall(void, "message", "variance = ", variance(xs))
//
(* ****** ****** *)

val ((*void*)) =
println! ("dotprod = ", dotprod_R34vector_R34vector(xs, xs))

(* ****** ****** *)

%{^
######
if
(!
(exists("libatscc2r34.is.loaded"))
)
{
  assign("libatscc2r34.is.loaded", FALSE)
}
######
if
(
!(libatscc2r34.is.loaded)
)
{
  sys.source("./../MYLIB/libatscc2r34_all.R"); sys.source("./../MYLIB/mylib_cats.R")
}
######
%} // end of [%{^]

(* ****** ****** *)

%{$
my_dynload()
%} // end of [%{$]

(* ****** ****** *)

(* end of [statfuns.dats] *)
