
//
#define
LIBATSCC2R34_targetloc
"$PATSHOME/contrib/libatscc2r34"
//
(* ****** ****** *)
//
#include "{$LIBATSCC2R34}/mylibies.hats"
//
(* ****** ****** *)

(*
fun
{a:t@ype}
R34vector_median
{n:pos}(R34vector(a, n)): double
//
overload median with R34vector_median
//
*)

(* ****** ****** *)

(*
fun
cbind_R34vector_R34vector
{a:t@ype}{n:pos}
(
xs: R34vector(a, n)
,
ys: R34vector(a, n)
) : R34matrix(a, n, 2) = "mac#cbind_R34vector_R34vector"
fun
rbind_R34vector_R34vector
{a:t@ype}{n:pos}
(
xs: R34vector(a, n)
,
ys: R34vector(a, n)
) : R34matrix(a, 2, n) = "mac#rbind_R34vector_R34vector"
//
overload cbind with cbind_R34vector_R34vector
overload rbind with rbind_R34vector_R34vector
//
*)
 
(* ****** ****** *)
(*
//
// HX-2017-10-11: incorporated
//
fun
dotprod_R34vector_R34vector
{a:t@ype}{n:pos}
(R34vector(a, n), R34vector(a, n)): (a) = "mac#dotprod_R34vector_R34vector"
//
*)
(* ****** ****** *)

