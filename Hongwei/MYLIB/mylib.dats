(*
** For implementing
** functions declared in mylib.sats
*)

(* ****** ****** *)

#staload "./mylib.sats"

(* ****** ****** *)
(*
//
implement
R34vector_median<int>(xs) =
$extfcall(double, "stats::median", xs)
//
implement
R34vector_median<double>(xs) =
$extfcall(double, "stats::median", xs)
//
*)
(* ****** ****** *)





(* ****** ****** *)
//
(* end of [mylib.dats] *)
